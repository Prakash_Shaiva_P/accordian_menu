//
//  main.m
//  AccordionTest
//
//  Created by Anoop M on 18/11/13.
//  Copyright (c) 2013 Anoop M. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AMPAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AMPAppDelegate class]));
    }
}
