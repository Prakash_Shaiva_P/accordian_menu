//
//  KSPObjects.m
//  AccordionTest
//
//  Created by Prakash Shaiva on 02/07/14.
//  Copyright (c) 2014 Anoop M. All rights reserved.
//

#import "KSPObjects.h"

@implementation KSPObjects
-(NSMutableArray*)children
{
    if (!_children) {
        _children = [[NSMutableArray alloc] init];
    }
    return _children;
}
@end
