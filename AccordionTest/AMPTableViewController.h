//
//  AMPTableViewController.h
//  AccordionTest
//
//  Created by Prakash Shaiva on 02/07/14.
//  Copyright (c) 2014 Anoop M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMPTableViewController : UITableViewController
{
    NSMutableArray *objArray;
    NSMutableArray *childArray;
    NSArray *filteredChildArray;
    NSInteger indentationlevel;
    CGFloat indendationWidth;
}
@end
