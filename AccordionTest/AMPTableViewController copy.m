//
//  AMPTableViewController.m
//  AccordionTest
//
//  Created by Anoop M on 18/11/13.
//  Copyright (c) 2013 Anoop M. All rights reserved.
//

#import "AMPTableViewController.h"
#import "AMPProducts.h"
@interface AMPTableViewController ()

@end

@implementation AMPTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    prodArray = [[NSMutableArray alloc] init];
    //childArray = [[NSMutableArray alloc] init];
    //filteredChildArray = [[NSArray alloc] init];
    indentationlevel = 0;
    indendationWidth = 20;
    // Create a sample array of parent objects
    for(int i=0;i<10;i++)
    {
        AMPProducts *prod = [[AMPProducts alloc] init];
        prod.name = [NSString stringWithFormat:@"Product%d",i];
        prod.parent = @"";
        prod.isExpanded = NO;
        prod.level = 0;
        // Randomly assign BOM status
        if(i%2)
        {
            prod.isBOM  = YES;
        }
        else
        {
            prod.isBOM = NO;
        }
        [prodArray addObject:prod];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [prodArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    AMPProducts *prod = [prodArray objectAtIndex:indexPath.row];
    cell.textLabel.text = prod.name;
    cell.detailTextLabel.text = prod.parent;
    cell.indentationLevel = prod.level;
    cell.indentationWidth = indendationWidth;
    // Show disclosure only if the cell can expand
    if(prod.isBOM)
    {
        cell.accessoryView = [self viewForDisclosureForState:prod.isExpanded];
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    // Configure the cell...
    
    return cell;
}
-(UIView*) viewForDisclosureForState:(BOOL) isExpanded
{
    NSString *imageName;
    if(isExpanded)
    {
        imageName = @"ArrowD_blue.png";
    }
    else
    {
        imageName = @"ArrowR_blue.png";
    }
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [myView addSubview:imgView];
    return myView;
}
// Utility class to create childrens for a selected parent class
-(void) fetchChildrenforParent:(AMPProducts*) parentProduct
{
    // If BOM then only we need to create child
    if(parentProduct.isBOM)
    {
        // The children property of the parent will be filled with this objects
        for(int i=0;i<10;i++)
        {
            AMPProducts *prod = [[AMPProducts alloc] init];
            prod.name = [NSString stringWithFormat:@"Child %d",i];
            prod.parent = [NSString stringWithFormat:@"Child %d of %@",i,parentProduct.name];
            // This is used for setting the indentation level so that it look like an accordion view
            prod.level  = parentProduct.level +1;
            prod.isExpanded = NO;
            
            if(i%2)
            {
                prod.isBOM = YES;
            }
            else
            {
                prod.isBOM = NO;
            }
            [parentProduct.children addObject:prod];
        }
    }
}


#pragma mark - Table view delegate
// Method to collapse the cell if it is already expanded
- (void)collapseCellsFromIndexOf:(AMPProducts *)prod indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    // Find the number of childrens opened under the parent recursively as there can be expanded children also
    int collapseCol = [self numberOfCellsToBeCollapsed:prod];
    
    // Find the range from the parent index and the length to be removed.
    NSRange collapseRange = NSMakeRange(indexPath.row+1, collapseCol);
    // Remove all the objects in that range from the main array so that number of rows are maintained properly
    [prodArray removeObjectsInRange:collapseRange];
    prod.isExpanded = NO;
    // Create index paths for the number of rows to be removed
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    for (int i = 0; i<collapseRange.length; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:collapseRange.location+i inSection:0]];
    }
    // Animate and delete
    [tableView deleteRowsAtIndexPaths:indexPaths
                     withRowAnimation:UITableViewRowAnimationLeft];
}

// Method to collapse the cell if it is already expanded
- (void)expandCellsFromIndexOf:(AMPProducts *)prod tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    // Create dummy children
    [self fetchChildrenforParent:prod];
    // Expand only if children are available
    if([prod.children count]>0)
    {
        prod.isExpanded = YES;
        int i =0;
        // Insert all the child to the main array just after the parent
        for (AMPProducts *prod1 in prod.children) {
            [prodArray insertObject:prod1 atIndex:indexPath.row+i+1];
            i++;
        }
        // Find the range for insertion
        NSRange expandedRange = NSMakeRange(indexPath.row, i);
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        // Create index paths for the range
        for (int i = 0; i<expandedRange.length; i++) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:expandedRange.location+i+1 inSection:0]];
        }
        // Insert the rows
        [tableView insertRowsAtIndexPaths:indexPaths
                         withRowAnimation:UITableViewRowAnimationLeft];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AMPProducts *prod = [prodArray objectAtIndex:indexPath.row];
    if(prod.isBOM)
    {
        
        if(prod.isExpanded)
        {
            
            [self collapseCellsFromIndexOf:prod indexPath:indexPath tableView:tableView];
        }
        else{
            NSLog(@"Can be expanded");
            [self expandCellsFromIndexOf:prod tableView:tableView indexPath:indexPath];
            
        }
        
    }
    
}

// Find the number of cells to be collapsed
-(int) numberOfCellsToBeCollapsed:(AMPProducts*) products
{
    int total = 0;
    if(products.isExpanded)
    {
        // Set the expanded status to no
        products.isExpanded = NO;
        NSMutableArray *child = products.children;
        total = child.count;
        // traverse through all the children of the parent and get the count.
        for(AMPProducts *prod in child)
        {
            total += [self numberOfCellsToBeCollapsed:prod];
        }
    }
    return total;
}
@end
