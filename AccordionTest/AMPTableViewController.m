//
//  KSPTableViewController.m
//  AccordionTest
//
//  Created by Prakash Shaiva on 02/07/14.
//  Copyright (c) 2014 Anoop M. All rights reserved.
//

#import "AMPTableViewController.h"
#import "KSPObjects.h"
@interface AMPTableViewController ()

@end

@implementation AMPTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    objArray = [[NSMutableArray alloc] init];
    //childArray = [[NSMutableArray alloc] init];
    //filteredChildArray = [[NSArray alloc] init];
    indentationlevel = 0;
    indendationWidth = 20;
    // Create a sample array of parent objects
    for(int i=0;i<10;i++)
    {
        KSPObjects *obj = [[KSPObjects alloc] init];
        obj.name = [NSString stringWithFormat:@"Parent%d",i];
        obj.parent = @"";
        obj.isExpanded = NO;
        obj.level = 0;
        // Randomly assign canBeExpanded  status
        if(i%2)
        {
            obj.canBeExpanded  = YES;
        }
        else
        {
            obj.canBeExpanded = NO;
        }
        [objArray addObject:obj];
    }

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [objArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    KSPObjects *obj = [objArray objectAtIndex:indexPath.row];
    cell.textLabel.text = obj.name;
    cell.detailTextLabel.text = obj.parent;
    cell.indentationLevel = obj.level;
    cell.indentationWidth = indendationWidth;
    // Show disclosure only if the cell can expand
    if(obj.canBeExpanded)
    {
        cell.accessoryView = [self viewForDisclosureForState:obj.isExpanded];
    }
    else
    {
        //cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = nil;
    }
    // Configure the cell...
    
    return cell;
}
// Show the arrow down if the row is expanded
-(UIView*) viewForDisclosureForState:(BOOL) isExpanded
{
    NSString *imageName;
    if(isExpanded)
    {
        imageName = @"ArrowD_blue.png";
    }
    else
    {
        imageName = @"ArrowR_blue.png";
    }
    UIView *myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [imgView setFrame:CGRectMake(0, 6, 24, 24)];
    [myView addSubview:imgView];
    return myView;
}
// Utility class to create childrens for a selected parent class
-(void) fetchChildrenforParent:(KSPObjects*) parentobjects
{
    // If canBe Expanded then only we need to create child
    if(parentobjects.canBeExpanded)
    {
        // If Children are already added then no need to add again
        if([parentobjects.children count]>0)
            return;
        // The children property of the parent will be filled with this objects
        for(int i=0;i<10;i++)
        {
            KSPObjects *obj = [[KSPObjects alloc] init];
            obj.name = [NSString stringWithFormat:@"Child %d",i];
            // This is used for setting the indentation level so that it look like an accordion view
            obj.level  = parentobjects.level +1;
            obj.parent = [NSString stringWithFormat:@"Child %d of Level %d",i,obj.level];
            

            obj.isExpanded = NO;
            
            if(i%2)
            {
                obj.canBeExpanded = YES;
            }
            else
            {
                obj.canBeExpanded = NO;
            }
            [parentobjects.children addObject:obj];
        }
    }
}


#pragma mark - Table view delegate
// Method to collapse the cell if it is already expanded
- (void)collapseCellsFromIndexOf:(KSPObjects *)obj indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    // Find the number of childrens opened under the parent recursively as there can be expanded children also
    int collapseCol = [self numberOfCellsToBeCollapsed:obj];
    
    // Find the range from the parent index and the length to be removed.
    NSRange collapseRange = NSMakeRange(indexPath.row+1, collapseCol);
    // Remove all the objects in that range from the main array so that number of rows are maintained properly
    [objArray removeObjectsInRange:collapseRange];
    obj.isExpanded = NO;
    // Create index paths for the number of rows to be removed
    NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
    for (int i = 0; i<collapseRange.length; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:collapseRange.location+i inSection:0]];
    }
    // Animate and delete
    [tableView deleteRowsAtIndexPaths:indexPaths
                     withRowAnimation:UITableViewRowAnimationLeft];
}

// Method to Expand the cell
- (void)expandCellsFromIndexOf:(KSPObjects *)obj tableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    // Create dummy children
    [self fetchChildrenforParent:obj];
    // Expand only if children are available
    if([obj.children count]>0)
    {
        obj.isExpanded = YES;
        int i =0;
        // Insert all the child to the main array just after the parent's index
        for (KSPObjects *obj1 in obj.children) {
            [objArray insertObject:obj1 atIndex:indexPath.row+i+1];
            i++;
        }
        // Find the range for insertion
        NSRange expandedRange = NSMakeRange(indexPath.row, i);
        NSMutableArray *indexPaths = [[NSMutableArray alloc] init];
        // Create index paths for the range
        for (int i = 0; i<expandedRange.length; i++) {
            [indexPaths addObject:[NSIndexPath indexPathForRow:expandedRange.location+i+1 inSection:0]];
        }
        // Insert the rows
        [tableView insertRowsAtIndexPaths:indexPaths
                         withRowAnimation:UITableViewRowAnimationLeft];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    KSPObjects *obj = [objArray objectAtIndex:indexPath.row];
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    if(obj.canBeExpanded)
    {
        
        if(obj.isExpanded)
        {
            
            [self collapseCellsFromIndexOf:obj indexPath:indexPath tableView:tableView];
            selectedCell.accessoryView = [self viewForDisclosureForState:NO];
        }
        else{
            NSLog(@"Can be expanded");
            [self expandCellsFromIndexOf:obj tableView:tableView indexPath:indexPath];

            selectedCell.accessoryView = [self viewForDisclosureForState:YES];
            
        }
        
    }
    
}

// Find the number of cells to be collapsed
-(int) numberOfCellsToBeCollapsed:(KSPObjects*) objects
{
    int total = 0;
    if(objects.isExpanded)
    {
        // Set the expanded status to no
        objects.isExpanded = NO;
        NSMutableArray *child = objects.children;
        total = child.count;
        // traverse through all the children of the parent and get the count.
        for(KSPObjects *obj in child)
        {
            total += [self numberOfCellsToBeCollapsed:obj];
        }
    }
    return total;
}
@end
