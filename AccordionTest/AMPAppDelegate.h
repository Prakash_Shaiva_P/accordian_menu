//
//  AMPAppDelegate.h
//  AccordionTest
//
//  Created by Prakash Shaiva on 02/07/14.
//  Copyright (c) 2014 Anoop M. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
