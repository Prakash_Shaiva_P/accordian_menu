//
//  AMPobjucts.h
//  AccordionTest
//
//  Created by Anoop M on 18/11/13.
//  Copyright (c) 2013 Anoop M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AMPobjucts : NSObject
{
    
}
@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *parent;
@property(nonatomic) BOOL isBOM;
@property(nonatomic) BOOL isExpanded;
@property(nonatomic) NSInteger level;
@property(nonatomic, strong) NSMutableArray *children;
@end
