//
//  KSPObjects.h
//  AccordionTest
//
//  Created by Prakash Shaiva on 02/07/14.
//  Copyright (c) 2014 Anoop M. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSPObjects : NSObject

@property(nonatomic,strong) NSString *name;
@property(nonatomic,strong) NSString *parent;
@property(nonatomic) BOOL canBeExpanded;
@property(nonatomic) BOOL isExpanded;
@property(nonatomic) NSInteger level;
@property(nonatomic, strong) NSMutableArray *children;

@end
