//
//  AMPobjucts.m
//  AccordionTest
//
//  Created by Anoop M on 18/11/13.
//  Copyright (c) 2013 Anoop M. All rights reserved.
//

#import "AMPobjucts.h"

@implementation AMPobjucts

-(NSMutableArray*)children
{
    if (!_children) {
        _children = [[NSMutableArray alloc] init];
    }
    return _children;
}
@end
